# Informações Gerais

Esse repositório contém a história 1 referente a JOY na estrutura OCC, desenvolvido para o Projeto Final da disciplina IA369Y (Computação Afetiva).

Além da história propriamente dita, criada em Twine 2 (Harlowe 3.x), também haverá outros arquivos com stylesheet e 
ferramentas de teste que serão implementadas à história próximo a sua conclusão.

# Narrativa

Informações mais detalhadas da Narrativa movidos para a wiki.


## Músicas do Agnus Dei

Todas as trilhas selecionadas para a narrativa Agnus Dei, foram obtidas em: http://soundimage.org/
